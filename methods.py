import numpy as np
from sklearn.metrics import mean_squared_error
from scipy.sparse import csr_matrix
from sklearn.base import BaseEstimator


class RidgeRegression(BaseEstimator):
    """Implementation of Ridge Regression"""
    def __init__(self, ld=0.2):
        self.ld = ld

    def fit(self, X, y):
        self.w = np.linalg.solve( X.T.dot(X) + self.ld * np.eye(X.shape[1]), X.T.dot(y) )

    def predict(self, X):
        return X.dot(self.w)

    def score(self, X, y):
        return mean_squared_error(self.predict(X), y)


class NeighborhoodItem:
    """Implementation of Neighborhood item-oriented approach"""
    def __init__(self, n=3951):
        self.n = n

    def fit(self, UI, UI_m):
        UI_copy = UI_m.copy()
        UI_copy.data **= 2
        tmp = np.sqrt(UI_copy.sum(axis=0))
        self.sim = np.nan_to_num((UI_m.T * UI_m) / (tmp.T * tmp))
        self.sim[self.sim < 0] = 0
        self.sim = np.asarray(self.sim)
        self.UI = UI
        sortd = np.sort(self.sim, axis=1)[:,::-1]
        sortd = sortd[:,self.n]
        self.sim[self.sim < sortd[:,np.newaxis]] = 0

    def predict(self, X):
        ans = np.empty(X.shape[0])
        UIvalues = [None] * self.UI.shape[0]
        UIidx = [None] * self.UI.shape[0]

        for u in range(self.UI.shape[0]):
            UIvalues[u] = self.UI[u].data.T
            UIidx[u] = self.UI[u].nonzero()[1]

        for k, elem in enumerate(X):
            u, i = elem
            ans[k] = np.sum(self.sim[i][UIidx[u]] * UIvalues[u]) / np.sum(self.sim[i][UIidx[u]])

        return np.nan_to_num(ans)

    def score(self, X, y):
        return mean_squared_error(self.predict(X), y)


class LatentFactor(BaseEstimator):
    """Implementation of latent factor approach in collaborative filtering"""
    def __init__(self, lp=0.2, lq=0.001, n=20, k=10):
        self.lp = lp
        self.lq = lq
        self.n = n
        self.k = k

    def fit(self, X, y):
        num_users, num_items = 6040, 3952
        self.Q = 0.1 * np.random.random((num_items, self.k))
        self.P = 0.1 * np.random.random((num_users, self.k))
        UIvalues = [np.empty(0) for _ in range(num_users)]
        UIidx = [np.empty(0, dtype=np.int) for _ in range(num_users)]
        IUvalues = [np.empty(0) for _ in range(num_items)]
        IUidx = [np.empty(0, dtype=np.int) for _ in range(num_items)]

        for (u, i), r in zip(X, y):
            UIvalues[u] = np.append(UIvalues[u], np.array(r))
            UIidx[u] = np.append(UIidx[u], np.array(i, dtype=np.int))
            IUvalues[i] = np.append(IUvalues[i], np.array(r))
            IUidx[i] = np.append(IUidx[i], np.array(u, dtype=np.int))

        for k in range(self.n):
            for u in range(num_users):
                if UIvalues[u].shape[0] == 0:
                    continue
                Qu = self.Q[UIidx[u]]
                Au = Qu.T.dot(Qu)
                du = Qu.T.dot(UIvalues[u])
                nu = UIidx[u].shape[0]
                self.P[u] = np.linalg.solve(Au + self.lp * nu * np.eye(Au.shape[0]), du).squeeze()
            for i in range(num_items):
                if IUvalues[i].shape[0] == 0:
                    continue
                Pi = self.P[IUidx[i]]
                Ai = Pi.T.dot(Pi)
                di = Pi.T.dot(IUvalues[i])
                ni = IUidx[i].shape[0]
                self.Q[i] = np.linalg.solve(Ai + self.lq * ni * np.eye(Ai.shape[0]), di).squeeze()

    def predict(self, X):
        ans = np.empty(X.shape[0])
        for k, elem in enumerate(X):
            u, i = elem
            ans[k] = self.P[u].T.dot(self.Q[i])
        return ans

    def score(self, X, y):
        return mean_squared_error(self.predict(X), y)
